import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { LocationComponent } from './components/location/location.component';
import { ForecastComponent } from './components/forecast/forecast.component';
import { ForecastLiteComponent } from './components/forecast-lite/forecast-lite.component';

@NgModule({
  declarations: [
    AppComponent,
    LocationComponent,
    ForecastComponent,
    ForecastLiteComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
