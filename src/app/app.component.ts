import { Component, OnInit } from '@angular/core';
import { finalize } from 'rxjs/operators';

import { Location } from './dto/location';
import { WeatherService } from './providers/weather.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  hasError: boolean;
  locations: Array<Location>;
  refreshing: boolean;

  constructor(private _weatherService: WeatherService) {
  }

  ngOnInit(): void {
    this.hasError = false;
    this.refreshing = true;
    this._weatherService.getWeatherConditions()
    .pipe(finalize(() => this.refreshing = false))
    .subscribe(() => {
      this.locations = this._weatherService.locations;
    }, error => {
      console.error(error);
      this.hasError = true;
    });
  }
}
