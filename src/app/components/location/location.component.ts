import { Component, OnInit, Input } from '@angular/core';
import { finalize } from 'rxjs/operators';

import { Forecast } from './../../dto/forecast';
import { Location } from './../../dto/location';

@Component({
  selector: 'app-location',
  templateUrl: './location.component.html',
  styleUrls: ['./location.component.scss']
})
export class LocationComponent implements OnInit {

  @Input() refreshing: boolean;
  @Input() location: Location;

  constructor() {
  }

  ngOnInit() {
  }
}
