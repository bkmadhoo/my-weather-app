import { Component, OnInit, Input } from '@angular/core';

import { Forecast } from '../../dto/forecast';

@Component({
  selector: 'app-forecast-lite',
  templateUrl: './forecast-lite.component.html',
  styleUrls: ['./forecast-lite.component.scss']
})
export class ForecastLiteComponent implements OnInit {

  @Input() forecast: Forecast;
  @Input() index: number;

  background: string;
  day: string;

  private _days: string[] = ['SUN', 'MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT'];

  constructor() {
    this.day = '';
  }

  ngOnInit() {
    this.background = `rgba(0, 0, 0, ${0.2 + (0.1 * this.index)})`;
    this.day = this._days[new Date(this.forecast.date * 1000).getDay()];
  }
}
