import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ForecastLiteComponent } from './forecast-lite.component';

describe('ForecastLiteComponent', () => {
  let component: ForecastLiteComponent;
  let fixture: ComponentFixture<ForecastLiteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ForecastLiteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ForecastLiteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
