import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';

import { Forecast } from './../../dto/forecast';
import { Location } from './../../dto/location';

@Component({
  selector: 'app-forecast',
  templateUrl: './forecast.component.html',
  styleUrls: ['./forecast.component.scss']
})
export class ForecastComponent implements OnInit {

  date: string;
  day: string;

  @Input() location: Location;

  private _days: string[] = ['SUNDAY', 'MONDAY', 'TUESDAY', 'WEDNESDAY', 'THURSDAY', 'FRIDAY', 'SATURDAY'];
  private _months: string[] = ['JANUARY', 'FEBRUARY', 'MARCH', 'APRIL', 'MAY', 'JUNE', 'JULY', 'AUGUST', 'SEPTEMBER',
   'OCTOBER', 'NOVEMBER', 'DECEMBER'];

  constructor() { }

  ngOnInit() {
    const date: Date = new Date(this.location.current.date * 1000);
    this.date = `${this._months[date.getMonth()]}, ${date.getDate()}`;
    this.day = this._days[date.getDay()];
  }
}
