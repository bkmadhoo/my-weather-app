export interface Forecast {
  condition: number;
  date: number;
  description: string;
  humidity?: number;
  maxTemperature?: number;
  minTemperature?: number;
  pressure?: number;
  temperature?: number;
}
