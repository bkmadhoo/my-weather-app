import { Forecast } from './forecast';

export interface Location {
  date: Date;
  current?: Forecast;
  readonly id: number;
  name: string;
  forecasts?: Forecast[];
}
