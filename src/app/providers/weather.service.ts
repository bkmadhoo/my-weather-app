import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable, forkJoin } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { Forecast } from './../dto/forecast';
import { Location } from './../dto/location';
import { environment } from './../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class WeatherService {

  private _locations: Location[];

  constructor(private _httpClient: HttpClient) {
    this._locations = [{
      date: new Date(),
      id: 934570,
      name: 'Curepipe'
    }, {
      date: new Date(),
      id: 934488,
      name: 'Goodlands'
    }];
  }

  get locations(): Location[] {
    return this._locations;
  }

  public getWeatherConditions(): Observable<any> {
    const observables$: Observable<void>[] = [];
    this._locations.forEach((location: Location) => {
      observables$.push(this.getCurrentWeatherForLocation(location));
      observables$.push(this.getWeatherForecastsForLocation(location));
    });
    return forkJoin(observables$);
  }

  private getCurrentWeatherForLocation(location: Location): Observable<any> {
    return this._httpClient.get(environment.url + '/weather', { params: this.getParams(location.id) })
      .pipe(map((data: any): void => {
        location.current = {
          condition: data.weather[0].id,
          date: data.dt,
          description: data.weather[0].main,
          humidity: data.main.humidity,
          maxTemperature: data.main.temp_max,
          minTemperature: data.main.temp_min,
          pressure: data.main.pressure,
          temperature: data.main.temp
        };
      }));
  }

  private getParams(locationId: number): HttpParams {
    return new HttpParams()
      .set('APPID', environment.weatherApiId)
      .set('id', locationId.toString())
      .set('units', 'metric');
  }

  private getWeatherForecastsForLocation(location: Location): Observable<any> {
    return this._httpClient.get(environment.url + '/forecast/daily', { params: this.getParams(location.id) })
      .pipe(map((data: any): void => {
        const forecasts: Forecast[] = [];
        data.list.forEach((element: any, index: number) => {
          if (index > 0 && index < 5) {
            forecasts.push({
              condition: element.weather[0].id,
              date: element.dt,
              description: element.weather[0].main,
              humidity: element.humidity,
              maxTemperature: element.temp.max,
              minTemperature: element.temp.min,
              pressure: element.pressure
            });
          }
        });
        location.forecasts = forecasts;
      }));
  }
}
